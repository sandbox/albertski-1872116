CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Use Case
 * Maintainers


Introduction
------------
This module add the ability to map taxonomy terms to a path or to other taxonomy
terms.


REQUIREMENTS
------------
 * This module does not have any requirements.


Installation
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
You can set the mappings at /admin/config/user-interface/taxonomy-map.

Mapping a taxonomy term to another taxonomy term:

  1. Scroll down to the New Mapping section.
  2. Choose a Taxonomy term in the "TAXONOMY" column.
  3. Choose a Taxonomy term from the "MAPPED TAXONOMY TERM" column.
  4. Select Save configuration.

Mapping a path to a taxonomy term:

  1. Scroll down to the New Mapping section.
  2. Type in path in the "PATH" column.
  3. Choose a Taxonomy term from the "MAPPED TAXONOMY TERM" column.
  4. Select Save configuration.


Use Case
--------

You have two node types that use two separate vocabularies that are somewhat
related.  You would configure the mappings of these terms. Then a developer
would most likely use taxonomy_map_get_mapping() in their custom module to get the
mapping.


MAINTAINERS
-----------
Current maintainer:
 * Albert Jankowski (albertski) - https://www.drupal.org/u/albertski
